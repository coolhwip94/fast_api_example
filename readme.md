# FastAPI Example
> This repo will serve as an example for FastAPI and showcase some capabilities
> FastAPI uses pydantic modeling, pydantic_reference/pydantic_example.py can be usd as a reference for a quick overview of pydantic

## Setup
---
- Create virtual env and install requirements
  ```
  python -m venv venv
  source venv/bin/activate
  pip install -r requirements.txt
  ```

## Running
---

- Run application with `uvicorn`
    ```
    uvicorn main:app --reload
    ```
    > app can be run on different ports using "--port" flag, default is port `8000`
    
    ```
    uvicorn main:app --reload --port 5000
    ```

## Paths
---
> This repo will showcase the concatenation of multiple paths or resources into a single app  

- `routers/users.py` : Simple example for pseudo manipulating user entries
    - Things to note with this example : pydantic modeling, response_model, and input fields
---
- `routers/items.py`: simple example for pseudo manipulating items records
    - Things to note : pydanitc modeling, response_model, HTTPExceptions, response based on query
    > This example showcases queries and responses based on query responses  
    > This uses simple list/dicts but can be expanded upon to read from databases or data sources
---  
<br> 
<br>


## Documentation
---
- FastAPI provides multiple forms of documentation.
  - `Swagger` : http://`<localhost/ip_address/hostname>`/docs
  - `Redoc` : http://`<localhost/ip_address/hostname>`/redoc
  - `Raw OpenAPI Json` : http://`<localhost/ip_address/hostname>`/openapi.json

## References
---
- `FastAPI` : https://fastapi.tiangolo.com/tutorial/
<br>

- `Redoc` : https://github.com/Redocly/redoc
> Redoc is an open-source tool for generating documentation from OpenAPI (fka Swagger) definitions.

- `uvicorn` : https://www.uvicorn.org
> Uvicorn is a lightning-fast ASGI server implementation, using uvloop and httptools.

- `pydantic` : https://pydantic-docs.helpmanual.io
> Data validation and settings management using python type annotations.