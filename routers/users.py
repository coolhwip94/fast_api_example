from fastapi import APIRouter
from pydantic import BaseModel


# model for input
class UserIn(BaseModel):
    username: str
    description: str = None
    password: str


# Model for output
class UserOut(BaseModel):
    username: str
    description: str = None


router = APIRouter(
    prefix='/users',
    tags=['users'],
    responses={
        404: {"description": "Not Found"}
    }
)


# User out model is used as the response model
# this documents on the swagger ui for the expected output
@router.get("/", response_model=UserOut)
# using UserIn class here, tells fastapi to expect those fields as input
async def read_users(user_info: UserIn):
    return [{"username": "Rick"}, {"username": "Morty"}]


@router.get("/me")
async def read_user_me():
    return {"username": "fakecurrentuser"}


@router.get("/{username}")
async def read_user(username: str):
    return {"username": username}
