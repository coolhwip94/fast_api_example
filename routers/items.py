from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from pydantic import BaseModel
from typing import List


# pydantic model for items
class ItemModel(BaseModel):
    item_id: int
    description: str = None
    name: str


# response model for retrieving all items
class Items(BaseModel):
    items: List[ItemModel]


router = APIRouter(
    prefix='/items',
    tags=['items'],  # referred to for documention or metadata
    responses={
        404: {"description": "Not Found"}
    })

# example data
items_data = [
    ItemModel(**{
        'item_id': 1,
        'description': 'item number 1',
        'name': 'name1',
    }),
    ItemModel(**{
        'item_id': 2,
        'description': 'item number 2',
        'name': 'name3',
    }),
]


@router.get("/", response_model=Items)
async def get_items():
    # return object has to match the class defined in response_model
    return {'items': items_data}

# get by providing an item id
@router.get("/{item_id}")
async def get_item_by_id(item_id: int):

    # query for item id
    matching_items = [
        item_data for item_data in items_data if item_data.item_id == item_id]

    # if item exists, return the matching item
    if len(matching_items) > 0:
        return matching_items[0]
    else:
        # if item does not exist, return 404
        raise HTTPException(
            404, detail=f"Resource not found for item_id : {item_id}")

# post request for ading new item
@router.post("/", responses={409: {"description": "Conflict: Resource already exists"}})
async def post_item(new_item_data: ItemModel):

    # check if item_id already exists (normally this can be a db query)
    existing_ids = [item_data.item_id for item_data in items_data]

    # return message indicating conflict if it already exists
    if new_item_data.item_id in existing_ids:
        raise HTTPException(
            409, detail=f"This item_id : {new_item_data.item_id} already exists")
    else:
        # add new object to dict
        items_data.append(new_item_data)

    # return newly added object
    return new_item_data
