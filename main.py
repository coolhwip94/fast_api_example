from fastapi import FastAPI
from routers import users, items


# Creating metadata for tags
tags_metadata = [
    {
        "name": "users",
        "description": "Operations for users"
    },
    {
        "name": "items",
        "description": "Crud Operations for items"
    }
]

app = FastAPI(version="1.0.0", description="Simple FastAPI Example", )

# Include Routers within app
app.include_router(users.router)
app.include_router(items.router)
